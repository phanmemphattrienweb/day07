<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Danh sách sinh viên</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css"
        rel="stylesheet" />

    <style>
    .background {
        width: 34rem;
        border: solid 2px #4e7aa3;
        margin: auto;
        margin-top: 2rem;
        padding: 0.6rem 0.8rem;
    }

    .search-bar {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
        padding: 1rem 2rem;
    }

    .list-form {
        font-size: 16px;
        width: 100%;
        display: flex;
    }

    .list-form-text {
        width: 5rem;
        padding: 0.6rem 0.6rem 0.2rem;
        /* margin-right: 0rem; */
        text-align: center;
    }

    .input-text {
        width: 16rem;
        height: 2.6rem;
        padding-left: 0.4rem;
        border: 2px solid #4e7aa3;
    }

    select {
        border: 2px solid #4e7aa3;
        padding: 0px;
        outline: none;
        width: 16rem;
        height: 2.6rem;
    }

    .list-search {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .btnSearch {
        height: 2.6rem;
        width: 6rem;
        background-color: #5b9bd5;
        border-radius: 10px;
        border: solid 2px #385e8b;
        cursor: pointer;
        color: white;
    }

    .search-res {
        display: flex;
        justify-content: flex-start;
    }

    .list-add {
        display: flex;
        justify-content: flex-end;
    }

    .btnAdd {
        height: 2rem;
        width: 4.8rem;
        background-color: #5b9bd5;
        border-radius: 5px;
        border: solid 2px #385e8b;
        cursor: pointer;
        color: white;
    }

    table {
        width: 100%;
    }

    td,
    th {
        text-align: left;
        padding: 0.6rem;
        margin-top: 1rem;
    }

    .list-student {
        margin-top: 0.2rem;
    }

    .btnAction {
        width: 3rem;
        color: white;
        background-color: #92b1d6;
        border: solid 1px #4e7aa3;
        cursor: pointer;
    }
    </style>
</head>

<body>
    <?php
  if (!empty($_POST['btnAdd'])) {
    header("Location: ./register.php");
  }
  ?>
    <fieldset class="background">
        <form action="" method="POST" id="form" enctype="multipart/form-data">

            <div class="search-bar">
                <div class="list-form">
                    <p class="list-form-text">
                        Khoa
                    </p>

                    <select name='department'>
                        <?php 
                $depart = array("EMPTY"=>"", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                foreach ($depart as $key => $value) {
                    echo '<option >' . $value . '</option>';
                  }
                  ?>
                    </select>
                </div>

                <div class="list-form">
                    <p class="list-form-text">
                        Từ khóa
                    </p>
                    <input type="text" name="keyWord" id="keyWord" class="input-text">
                </div>

                <div class="list-form list-search">
                    <input type="submit" value="Tìm kiếm" name="btnSearch" class="btnSearch">
                </div>
            </div>

            <div class="search-res">
                <p>Số sinh viên tìm thấy: XXX</p>
            </div>

            <div class="list-add">
                <input type="submit" class="btnAdd" name="btnAdd" value="Thêm" />
            </div>

            <div class="list-student">
                <table>
                    <tr>
                        <th>No</th>
                        <th>Tên sinh viên</th>
                        <th>Khoa</th>
                        <th>Action</th>
                    </tr>

                    <tr>
                        <td>1</td>
                        <td>Nguyễn Văn A</td>
                        <td>Khoa học máy tính</td>
                        <td>
                            <button class="btnAction">Xóa</button>
                            <button class="btnAction">Sửa</button>
                        </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Trần Thị B</td>
                        <td>Khoa học máy tính</td>
                        <td>
                            <button class="btnAction">Xóa</button>
                            <button class="btnAction">Sửa</button>
                        </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Nguyễn Hoàng C</td>
                        <td>Khoa học vật liệu</td>
                        <td>
                            <button class="btnAction">Xóa</button>
                            <button class="btnAction">Sửa</button>
                        </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Đinh Quang D</td>
                        <td>Khoa học vật liệu</td>
                        <td>
                            <button class="btnAction">Xóa</button>
                            <button class="btnAction">Sửa</button>
                        </td>
                    </tr>
                </table>
            </div>

        </form>
    </fieldset>


</body>

</html>